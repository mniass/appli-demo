FROM java:8
EXPOSE 8080
ADD /target/appli-demo-1.0-SNAPSHOT.jar appli-demo-1.0-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","demo.jar"]
